"""
Space Invaders!
"""
import pygame
import space_invaders_misc
import random

# Defining of colors to be used in the game.
BLACK  = (  0,   0,   0)
WHITE  = (255, 255, 255)
BLUE   = (100, 149, 237)
GREEN  = (  0, 204,   0)
ORANGE = (255, 165,   0)

# Setting of the screen size (width, height).
SCREEN_SIZE = (800, 600)

# Creation of the screen.
SCREEN = pygame.display.set_mode(SCREEN_SIZE)

# Caption for the game.
pygame.display.set_caption('Space Invaders!')

# Retrieving of the current high score.
with open('high_score_file', 'r') as high_score_file:
    high_score = int(high_score_file.read())

# Variable to determine how fast the screen updates, i.e. its frames per second.
FPS = pygame.time.Clock()

# Creation of a sprites list for all sprites in game.
all_sprites_list = pygame.sprite.Group()

# Creation of an enemy sprites list for all enemy sprites in game.
enemy_sprites_list = pygame.sprite.Group()

# Creation of a sprites list for the player sprite.
player_sprite_list = pygame.sprite.Group()

# Creation of the player sprite.
PLAYER = space_invaders_misc.Player(BLUE)

# Adding of the player sprite to its own list.
player_sprite_list.add(PLAYER)

# Adding of the player sprite to list of all sprites.
all_sprites_list.add(PLAYER)

# ----- Start generation of initial row of enemy sprites -----

# First enemy sprite is created at the left side of the screen.
enemy_x_pos = 0

# Each enemy sprite is created 100 pixels from the top of the screen.
enemy_y_pos = 100

# 20 enemy sprites are to be created
n_enemies = 20

# Creation of the enemy sprites.
for i in range(n_enemies):
    enemy = space_invaders_misc.generate_enemies(enemy_x_pos, enemy_y_pos)

    # Add each enemy sprite to list of enemy sprites.
    enemy_sprites_list.add(enemy)

    # Also to list of all sprites.
    all_sprites_list.add(enemy)

    # Create the next enemy 25 pixels to the right of the previous one.
    enemy_x_pos += 25

# ----- End generation of initial row of enemy sprites -----

def draw_game_over_screen(FONT, SCREEN):
    """ Function to draw the 'GAME OVER' screen. """

    # Rendering of the 'GAME OVER' text; whether its anti-aliased; its color.
    game_over_text = FONT.render('GAME OVER', True, WHITE)

    # Fetching the rectangular object with the dimensions of the image.
    game_over_text_rect = game_over_text.get_rect()

    # Determining the x-y placing of the text, in the middle of the screen.
    game_over_text_x = (SCREEN.get_width() / 2) - (game_over_text_rect.width / 2)
    game_over_text_y = (SCREEN.get_height() / 2) - \
                               (game_over_text_rect.height / 2)

    # Placing the image of the text in the middle of the screen.
    SCREEN.blit(game_over_text, (game_over_text_x, game_over_text_y))


# ------- Main game function. -------

def main():
    """ Main game function. """

    # Initialization of Pygame.
    pygame.init()

    # Selection of font to use; its size; whether its bolded; whether its italicized.
    FONT = pygame.font.SysFont('Calibri', 25, True, False)

    # Condition to end game.
    game_ended = False

    # Variable to determine if the game is over,
    # i.e. if the player has been hit by the enemy sprite or bullet.
    game_over = False

    # Setting of initial score to be 0.
    score = 0

    # ----- Start code for sound effects/background music -----

    # Sound effect for the player-fired bullet.
    player_firing_sound = pygame.mixer.Sound('shoot.wav')

    # Setting of the volume for the player-fired bullet sound effect.
    player_firing_sound.set_volume(0.10)

    # Sound effect for the enemy sprite's destruction.
    enemy_destroyed_sound = pygame.mixer.Sound('invaderkilled.wav')

    # Setting of the volume for the enemy sprite's destruction sound effect.
    enemy_destroyed_sound.set_volume(0.10)

    # Sound effect for the player sprite's defeat.
    player_defeated_sound = pygame.mixer.Sound('explosion.wav')

    # Setting of the volume for the player sprite's destruction sound effect.
    player_defeated_sound.set_volume(0.10)

    # Background music for the game.
    pygame.mixer.music.load('bgm.wav')

    # Setting of the volume of the background music.
    pygame.mixer.music.set_volume(0.50)

    # Setting of the 'end event' that Pygame sends when the music stops.
    pygame.mixer.music.set_endevent(pygame.constants.USEREVENT)

    # Playing of the background music.
    pygame.mixer.music.play()

    # ----- End code for sound effects/background music -----

    # Setting of initial absence of player-fired bullet.
    bullet_player_present = False

    # Setting of initial absence of enemy-fired bullet.
    bullet_enemy_present = False

    # Setting of initial level to 1.
    current_level = 1

    # Variable to determine if the current level has been cleared.
    level_cleared = False

    # -------- Start Main Programme Loop --------

    # Whilst user has not closed the game window:
    while not game_ended:
        for event in pygame.event.get():

            # If user closes game window:
            if event.type == pygame.QUIT:
                game_ended = True

            # Setting the speed of the player sprite based on the key pressed.
            elif event.type == pygame.KEYDOWN:

                # Changing the speed of the player sprite
                # by a factor of 10 pixels accordingly when a key is pressed.
                if event.key == pygame.K_LEFT:
                    PLAYER.change_speed(-10)
                elif event.key == pygame.K_RIGHT:
                    PLAYER.change_speed(10)

                # If spacebar is pressed:
                elif event.key == pygame.K_SPACE:
                    if not bullet_player_present:

                        # Generation of the player-fired bullet sprite.
                        BULLET = space_invaders_misc.Bullet(GREEN,
                                                            PLAYER.rect.x + 10,
                                                            PLAYER.rect.y - 10,
                                                            'PLAYER')

                        # Adding of the bullet sprite to the list of all sprites.
                        all_sprites_list.add(BULLET)

                        # Setting the player-fired bullet to be present.
                        bullet_player_present = True

                        # Playing of the player's bullet-firing sound.
                        player_firing_sound.play()

            # Resetting the speed of the player sprite when a key is released.
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    PLAYER.change_speed(10)
                elif event.key == pygame.K_RIGHT:
                    PLAYER.change_speed(-10)

            elif event.type == pygame.constants.USEREVENT:

                # Load the background music again.
                pygame.mixer.music.load('bgm.wav')

                # Play the background music.
                pygame.mixer.music.play()

         # ----- Start checks for whether the bullet has been fired by the enemy -----

        # Generation of a random number.

        # Checking if there are enemies still alive in the game.
        if enemy_sprites_list:
            random_number = random.randrange(len(enemy_sprites_list))

        # Creation of a placeholder variable to match with the random number.
        i = 0
        if not bullet_enemy_present:
            for enemy in enemy_sprites_list:
                if i == random_number:

                    # Creation of a bullet fired by the enemy.
                    BULLET_enemy = space_invaders_misc.Bullet(ORANGE,
                                                              enemy.rect.x + 5,
                                                              enemy.rect.y + 10,
                                                              'ENEMY')

                    # Adding of the enemy-fired bullet sprite to the list of all sprites.
                    all_sprites_list.add(BULLET_enemy)

                    # Setting the enemy-fired bullet to be present.
                    bullet_enemy_present = True
                    break
                else:
                    i += 1

        # Checking if the bullet has been fired by the enemy.
        if bullet_enemy_present:
            # Creation of a list to check if the player has been hit.
            player_hit_list = pygame.sprite.spritecollide(BULLET_enemy,
                                                          player_sprite_list,
                                                          False)

            # Checking if the player has been hit.
            if player_hit_list:
                game_over = True

            # Checking if the bullet has gone beyond the screen,
            # i.e. missed its target.
            if BULLET_enemy.rect.y > 600:

                # If so, remove it from the game.
                all_sprites_list.remove(BULLET_enemy)
                bullet_enemy_present = False

         # ----- End checks for whether the bullet has been fired by the enemy -----

         # ----- Start checks for whether the bullet has been fired by the player -----

        # If the bullet has been fired:
        if bullet_player_present:

            # Creation of a list of all enemy sprites that the bullet has hit,
            # and removing them from the game.
            collided_list = pygame.sprite.spritecollide(BULLET, enemy_sprites_list, True)

            # Checking if any enemy sprites have been hit.
            if collided_list:
                all_sprites_list.remove(BULLET)
                bullet_player_present = False
                for enemy in collided_list:

                    # Playing of the sound that signals
                    # the destruction of an enemy sprite.
                    enemy_destroyed_sound.play()
                    score += 1

            # Checking if the bullet has gone beyond the screen,
            # i.e. missed its target.
            if BULLET.rect.y <= 0:

                # If so, remove it from the game.
                all_sprites_list.remove(BULLET)
                bullet_player_present = False

        # ----- End checks for whether the bullet has been fired by the player -----

        # Checking if the player has been hit by an enemy sprite

        for enemy in enemy_sprites_list:
            player_hit_enemy_list = pygame.sprite.spritecollide(enemy,
                                                                player_sprite_list,
                                                                False)
            if player_hit_enemy_list:
                game_over = True
                break

        # ----- Start level checks -----

        # Checking if the level has been cleared.
        if not level_cleared:

            # Checking if all enemy sprites currently on the screen have been destroyed.
            if not enemy_sprites_list.sprites():

                # The current level has been cleared.
                level_cleared = True

                # Increase the current level count.
                current_level += 1

                # ----- Start generation of next level of enemy sprites -----

                # Enemy sprite is first created 100 pixels from the top of the screen.
                enemy_y_pos = 100

                # Create 20 enemy sprites horizontally each row.
                n_enemies = 20
                for i in range(current_level):

                    # First enemy sprite for each row is created
                    # at the left side of the screen.
                    enemy_x_pos = 0

                    # Each succeeding row of enemy sprites is created 25 pixels
                    # below the preceding row.
                    enemy_y_pos += 25 * i

                    # Creation of enemy sprites.
                    for j in range(n_enemies):
                        enemy = space_invaders_misc.generate_enemies(enemy_x_pos,
                                                                     enemy_y_pos)

                        # Adding of each enemy sprite to the list of enemy sprites.
                        enemy_sprites_list.add(enemy)

                        # Also to the list of all sprites.
                        all_sprites_list.add(enemy)

                        # Creation of the next enemy 25 pixels to the right
                        # of the previous one.
                        enemy_x_pos += 25

                # ------ End generation of next level of enemy sprites -----

        # ----- End level checks -----

        # ----- Start checks to determine whether the right-most
        # or left-most sprite has hit either side of the screen -----

        for enemy in enemy_sprites_list:

            # Set a temporary variable to be the current enemy sprite.
            temp = enemy

            # Check if either the right-most or left-most sprite
            # has hit either side of the screen.
            if enemy.collide_wall:
                enemy.collide_wall = False
                for enemy_2 in enemy_sprites_list:

                    # Since the right-most or left-most enemy sprite (depending)
                    # has already been updated, don't update it again.
                    if enemy_2 != temp:
                        enemy_2.update_collide()
                break

        # ----- End checks to determine whether the right-most
        # or left-most sprite has hit either side of the screen -----

        # ----- Start drawing code -----

        # Clearing the screen of all existing drawings, and setting background color.
        SCREEN.fill(BLACK)

        # Whilst the game is not over:
        if not game_over:

            # Updating the game with the current location
            # of all sprites.
            all_sprites_list.update()
        else:

            # Drawing of the 'GAME OVER' screen.
            draw_game_over_screen(FONT, SCREEN)

            # Playing of the sound that signals the player's defeat.
            if player_defeated_sound != None:
                player_defeated_sound.play()
                player_defeated_sound = None

            # Writing of the current score to the high score file if
            # the current score is higher than the high score.
            if score > high_score:
                with open('high_score_file', 'w') as high_score_file:
                    high_score_file.write(str(score))

        # Drawing of all sprites currently in the game.
        all_sprites_list.draw(SCREEN)

        # Rendering of the high score; whether its anti-aliased; its color.
        high_score_text = FONT.render('HIGH SCORE: ' + str(high_score), True, WHITE)

        # Placing the image of the high score on the screen at x = 610, y = 10.
        SCREEN.blit(high_score_text, (610, 10))

        # Rendering of the score; whether its anti-aliased; its color.
        score_text = FONT.render('SCORE: ' + str(score), True, WHITE)

        # Placing the image of the score on the screen at x = 670, y = 40.
        SCREEN.blit(score_text, (670, 40))

        # Rendering of the current level number.
        level_text = FONT.render('LEVEL: ' + str(current_level), True, WHITE)

        # Placing the image of the level on the screen at x = 5, y = 10.
        SCREEN.blit(level_text, (5, 10))

        # Updating of the screen with what has been drawn so far.
        pygame.display.flip()

        # ----- End drawing code -----

        # Setting the FPS of the game.
        FPS.tick(60)

    # -------- End Main Programme Loop ----------

    # Ensuring that Pygame quits properly.
    pygame.quit()

if __name__ == '__main__':
    main()