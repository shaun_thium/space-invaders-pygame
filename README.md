# Instructions
1. [Optional] If using `conda`, create a new environment first i.e. `conda create -n space_invaders`, then switch to it `conda activate space_invaders`
2. Use `pip` to install `pygame`, i.e. `pip install pygame`
3. Run the game using `python space_invaders.py`