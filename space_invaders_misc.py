import pygame

RED = (255, 0, 0)

# ------- Classes -------

# ----- Class: Player sprite -----

class Player(pygame.sprite.Sprite):
    """ This is the player-controlled sprite class. """

    def __init__(self, color):
        """Initializes the player sprite with a chosen color,
        and corresponding variables."""

        super(Player, self).__init__()

        # Creation of the image of the player sprite, and setting of its width and height.
        self.image = pygame.Surface((30, 30))

        # Setting of the color of the player sprite based on the chosen color.
        self.image.fill(color)

        # Fetching of the rectangular object with the dimensions of the image.
        self.rect = self.image.get_rect()

        # Creation of the player sprite in the middle of the screen.
        self.rect.x = 385
        self.rect.y = 550

        # Setting of the initial speed of the player sprite.
        self.change_x = 0

    def change_speed(self, x):
        """ Changes the speed of the player sprite. """

        self.change_x += x

    def update(self):
        """ Updates location for the player sprite. """

        self.rect.x += self.change_x

        # Check to see if wall boundary has been hit:
        if self.rect.x > 770:
            self.rect.x = 770
        if self.rect.x < 0:
            self.rect.x = 0

# ----- Class: Bullet sprite -----

class Bullet(pygame.sprite.Sprite):
    """ This is the bullet class for both the player-controlled sprite
    and the enemy sprite. """

    def __init__(self, color, x, y, firer):
        """ Initializes the bullet sprite with a chosen color,
        and its corresponding variables. """

        super(Bullet, self).__init__()

        # Creation of the image of the bullet sprite, and setting its width and height.
        self.image = pygame.Surface((10, 10))

        # Setting the color based on chosen color.
        self.image.fill(color)

        # Fetching of the rectangular object with dimensions of the image.
        self.rect = self.image.get_rect()

        # Creation of the bullet sprite in the middle of
        # where either the player or the enemy sprite is.
        self.rect.x = x
        self.rect.y = y

        # Variable to determine if the bullet was fired by the player or enemy.
        self.fired = firer

    def update(self):
        """ Updates location for the bullet sprite. """

        # If the bullet was fired by the player,
        if self.fired == 'PLAYER':

            # The bullet moves upwards with a speed of 10.
            self.rect.y -= 10
        else:
            # Else, the bullet moves downwards with a speed of 5.
            self.rect.y += 5

# ----- Class: Enemy sprite -----

class Enemy(pygame.sprite.Sprite):
    """ This is the enemy sprite class. """

    def __init__(self, color, x, y):
        """ Initializes the enemy sprite with a chosen color,
        and its corresponding variables."""

        super(Enemy, self).__init__()

        # Creation of the image of the player sprite, and setting its width and height.
        self.image = pygame.Surface((20, 20))

        # Setting the color of the enemy sprite.
        self.image.fill(color)

        # Fetching of the rectangular object that has the dimensions of the image.
        self.rect = self.image.get_rect()

        # Setting the initial coordinates of the sprite.
        self.rect.x = x
        self.rect.y = y

        # Setting the initial speed and direction of sprite.
        self.change_x = 1

        # Variable to determine whether the enemy sprite has collided with the wall.
        self.collide_wall = False

    def update(self):
        """ Updates location for the enemy sprite. """

        # Checking to see if wall boundary has been hit.
        if 0 <= self.rect.x <= 780:
            self.rect.x += self.change_x

        elif self.rect.x > 780:
            self.rect.x = 780
            self.rect.y += 25

            # If right wall boundary hit, changing of direction of movement.
            self.change_x *= -1

            # Update that the enemy sprite has hit the wall.
            self.collide_wall = True

        elif self.rect.x < 0:
            self.rect.x = 0
            self.rect.y += 25

            # If left wall boundary hit, changing of direction of movement.
            self.change_x *= -1

            # Update that the enemy sprite has hit the wall.
            self.collide_wall = True

    def update_collide(self):
        """ Updates location if either wall has been hit. """

        if self.rect.x == 780:
            self.collide_wall = False
        elif self.rect.x == 0:
            self.collide_wall = False
        else:
            if self.change_x == 1:

                # Shift the entire row by 2 pixels accordingly,
                # depending on which direction the row is moving.
                self.rect.x -= 2

                # Shifting of the entire row down.
                self.rect.y += 25

                # Changing of direction of movement.
                self.change_x *= -1

            else:
                self.rect.x += 2

                # Shifting of the entire row down.
                self.rect.y += 25

                # Changing of direction of movement.
                self.change_x *= -1

# ------- Methods -------

def generate_enemies(x_pos, y_pos):
    """ Generate enemy sprites and add them to list of enemy sprites,
        as well as all sprites. """

    enemy = Enemy(RED, x_pos, y_pos)

    return enemy